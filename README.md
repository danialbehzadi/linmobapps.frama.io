# LINMOBapps

## LINux on MOBile Apps

### History
This project was served at [https://linmobapps.frama.io/](https://linmobapps.frama.io/) and has been forked from the great [https://mglapps.frama.io/](https://mglapps.frama.io) list always with the goal of creating something static site generator based with the name "LinuxPhoneApps.org". On March 28th, 2022, after more than one year of interim LINMOBapps existence, [LinuxPhoneApps.org](https://linuxphoneapps.org) was finally launched - but this is still the place where the .csv files that contain all the data are being stored and edited.

### Structure
Files:
* [index.html](index.html): Main page hosted at [https://linmobapps.frama.io/](https://linmobapps.frama.io/), redirects to LinuxPhoneApps.org now. If you really miss the old page, e.g. for its better search functionality, fear not, it still lives on [LinuxPhoneApps.org](https://linuxphoneapps.org/lists/apps-classic/)!
* [complete.csv](complete.csv): Complete list of apps, to maintain remerge-ability to MGLapps, no longer maintained,
* [apps.csv](apps.csv)__: Main app list (subset of complete.csv), to be edited directly. _Add your apps here ([instructions](https://linuxphoneapps.org/docs/contributing/edit-csv-in-libreoffice/))!_
* [apps-to-be-added.md](apps-to-be-added.md)__: Apps waiting to be added.
* [games.csv](games.csv)__: Main games list (subset of complete.csv), to be edited directly.
* [archive.csv](archive.csv): Retired apps (subset of complete.csv).
* [other games.csv](other apps.csv): Further games which are not added to the main list yet, because they are not in a usable state, are still in planning stage or wait to be transfered to the main list

### Checks

To help with maintenance, you can use our checkers. In order to run themi locally, you are going to need to have Python 3 installed and run 

~~~
python3 -m pip install -r requirements.txt
~~~

first. After that, it's a simple `python3 check_XY.py check FILE.csv`.

### Licensing
Just like its origin [MGLApps](https://mglapps.frama.io), LINux on MOBile Apps is licensed under CC BY-SA 4.0 International: [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/) . For more licensing information, see [LICENSE](LICENSE)

### Contributing
* If you want to help, check [apps-to-be-added.md](apps-to-be-added.md) for a list of known apps that might be work adding or just check the [open Issues](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues) for improvements to existing listings and feel free to open some new ones!
* If an app works for you and it's not listed yet, and you don't know how Merge Requests work, [head over there](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues/13)! 

If you don't want to sign up for Framagit (which is weird, because [FramaSoft](https://framasoft.org/en/) is really cool, and they support signing up with GitHub and GitLab.com, BTW), you can also send a patch to the relevant mailing list: [linuxphoneapps.org-devel](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel)
If you don't want to send a patch, you can also just [submit your app by email](https://linuxphoneapps.org/docs/contributing/submit-app-by-email/) and hope another contributor gets to it quickly :)

### Please test my app!
* If you have an app that might work on LinuxPhones, but don't have a device, [please head over here](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues/12)!
